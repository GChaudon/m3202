from hillcipher.util import *
import json
import sys

def clairConnu(messageClair, messageChiffre, n=2):
    if len(messageClair) != len(messageChiffre):
        raise Exception("Longueur du message clair different de la longueur du message chiffre")

    groupeClair = grouper(messageClair, n)
    groupeChiffre = grouper(messageChiffre, n)

    for i in range(len(groupeClair)-(n-1)):
        chiffre = transpose([
            transposerIndices(groupeChiffre[i]),
            transposerIndices(groupeChiffre[i+1])
        ])

        clair = transpose([
            transposerIndices(groupeClair[i]),
            transposerIndices(groupeClair[i+1])
        ])
        
        if inversible(clair):
            return dot(chiffre, inverseMatrice(clair)) % 26

    raise Exception("Cle non trouve")

def analyserMots():
    mots = []
    motsPossibles = []
    with open('hillcipher/mots.txt') as wordsFile:
        mots = wordsFile.read().split(' ')
    return mots

def getBigrammesFrequencies():
    data = ""
    with open('hillcipher/bigrammes.json') as frequenciesFile:
        data = json.loads(frequenciesFile.read())
    return data

def analyserBigrammes(ciphertext):
    frequenciesRaw = getBigrammesFrequencies()
    frequencies = {}

    # Decoupage du texte chiffre en bloc de 2
    blocs = [ciphertext[i:i+2] for i in range(0, len(ciphertext), 2)]

    # Ajout d'un eventuel Z si le texte chiffre est impair
    if len(blocs[-1]) == 1:
         blocs[-1].append('Z')

    # On compte le nombre d'occurences des bigrammes dans le texte chiffre
    for bigramme in blocs:
        if bigramme not in frequencies.keys():
            frequencies[bigramme] = 0
        frequencies[bigramme] += 1

    # print(sorted(frequencies.iteritems(), key = lambda x : x[1])[::-1])

    # On convertit le dictionnaire du nombre d'occurrences en dictionnaire des frequences
    for bigramme in frequencies.keys():
        frequencies[bigramme] = round(frequencies[bigramme] / float(len(ciphertext)), 4)

    # On trie les dictionnaires des frequences par ordre decroissant
    frequencies = sorted(frequencies.iteritems(), key = lambda x : x[1])[::-1]
    
    frequenciesRaw = sorted(frequenciesRaw.iteritems(), key = lambda x : x[1])[::-1]

    return (frequencies, frequenciesRaw)

def cryptanalyseStatistique(ciphertext, frequencies, frequenciesRaw, depth=5):

    # Decoupage du texte chiffre en bloc de 2
    blocs = [ciphertext[i:i+2] for i in range(0, len(ciphertext), 2)]

    # Recuperation et tri par ordre decroissant des frequences des bigrammes de la langue francaise
    frequenciesRaw = getBigrammesFrequencies()
    frequenciesRaw = sorted(frequenciesRaw.iteritems(), key = lambda x : x[1])[::-1]

    # Recuperation d'une liste de mots courants
    mots = ""
    with open("hillcipher/mots3.txt") as fichier:
        mots = fichier.read().split(" ")
    
    best = ""
    key = []
    bestcount = 0

    for i in range(len(blocs)-1):
        for j in range(depth):
            if blocs[i] == frequencies[j][0]:
                segment = blocs[i] + blocs[i+1]
                for b1 in frequenciesRaw[:depth]:
                    for b2 in frequenciesRaw:
                        try:
                            keyFound = clairConnu(b1[0] + b2[0], segment)
                            txt = DCrypteHill(ciphertext, keyFound)
                            count = 0
                            for mot in mots:
                                if mot in txt:
                                    if len(mot) >= 3:
                                        count += 1
                            if count >= bestcount:
                                best = txt
                                bestcount = count
                                key = keyFound
                                print("\n-------------------------Recherche de la meilleure correspondance-------------------------\n")
                                print(txt)
                        except Exception as e:
                            if "Cle non trouve" in str(e):
                                pass
    print("Meilleure correspondance trouvee: ")
    print(best)
    print(str(bestcount) + " mots correspondants")
    print("Cle utilisee:")
    print(key)

    return best