#!/usr/bin/python

from numpy import *
import operator

# Exemple et questions
# 1

def pgcd(a,b):
    if b==0:
        return a
    else:
        r=a%b
        return pgcd(b,r)

def inversible(matrix):
    det = int(linalg.det(matrix))
    return pgcd(det, 26) == 1

def brute_force():
    nombre = 0
    # On teste toutes les cles possibles pour une matrice 2 x 2 (26**(2**))
    # Soit 456 976 possibilites
    for a in range(26):
        for b in range(26):
            for c in range(26):
                for d in range(26):
                    # Si la matrice testee est bien inversible mod 26
                    if inversible([[a, b], [c, d]]):
                        nombre+=1
    return nombre

def compute_keys_number(size):
    n = 26**(size**2)
    for i in range(1, size+1):
        n *= (1 - 1/(26**i))
    return int(n)

# Renvoie le nombre d'entiers inversibles mod m entre 1 et m
def euler(m):
    if(m > 1):
        return len(inversibles(m))
    else:
        return 0

# Renvoie la liste d'entiers inversibles modulo m (premiers avec m) entre 1 et m
def inversibles(m): 
    inverses = []
    if m <= 1:
        return []
    for i in range(m):
        if pgcd(i, m) == 1:
            inverses.append(i)
    return inverses

# Renvoie le plus grand commun diviseur de deux nombres
def pgcd(a,b):
    if b==0:
        return a
    else:
        r=a%b
        return pgcd(b,r)

# Renvoie le nombre de cles approximatif pour une matrice de taille n x n
def keys_num(n):
    return 26**(n**2)*(euler(26) / 26.0)

# print("Nombre de matrices de taille 2 inversibles modulo 26: %d" % nombre)
# print("Nombre de matrices de taille 2 inversibles modulo 26: %d" % compute_keys_number(2))
# print("Nombre de matrices de taille 5 inversibles modulo 26: %d" % compute_keys_number(5))
# sprint("Nombre de matrices de taille 2 inversibles modulo 26 (brute force): %d" % brute_force())
# print("Fonction indicatrice d'euler jusqu'a 25 : %f" % (euler(25) / 26.0))
# print("%d" % keys_num(2))
# print("%d" % keys_num(3))

