#!/usr/bin/python3

from hillcipher.util import *
from hillcipher.cryptanalysis import *

B = array([[2, 3], 
           [7, 5]])

C = array([[1, 3, 2],
           [5, 3, 2],
           [7, 2, 5]])

D = array([[1, 3, 3],
           [5, 3, 2],
           [7, 2, 5]])

message1 = "HILL"

# Question 1
print("B est inversible : {}".format(inversible(B)))

# Question 2
print("C est inversible : {}".format(inversible(C)))
print("D est inversible : {}".format(inversible(D)))

# Question 3
print("Code de " + message1 + " : " + str(transposerAscii(message1)))
print("Message de " + str(transposerAscii(message1)) + " : " + transposerLettre(transposerAscii(message1)))

#print(CrypterHill('JE MAPELLE GUILLAUME', B))
print(DCrypteHill('UWGMWZRREIUB', [[9, 4], [5, 7]]))

#ciphertext = "HEAPD YTWCB ZOQOM ISTNI ZOAIS CCGHE GJGNR CJKFK ZOMIR CTWFK SJEUN WMCMI VRQOM SYUFO TKAJY UGZPT IHQHK MHEIP HEBTV CWVGJ GNUPA UMFGN RCWVF KFKJZ PCRCS AWZPT CBEUQ HRFBI WFMFP TMFPT EXIPA LWXYU QDSCZ SECMS MINIT WNFDO OLPCG ZLPLK HQEUF KHHUQ YPKHK CTNZS ZDSOO IXMPT LSNNP CNFQO MSCMF EUQRL MICMS OTWMS WZIWH AIDEC OYDOV RYAVA KMSOW UMIBI LCLKA LKMMI STICN QSOYK PCZOX MMFYB UJQYJ KD FUJYJ DQLYHU GKY D QLQYJ ZQCQYI DQLYWKU XEXU XEXU"

#print(compare(sortDictionary(analyze(ciphertext)), sortDictionary(french_frequencies), ciphertext))

