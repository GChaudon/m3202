french_frequencies = {
    'A': 8.25,
    'B': 1.25,
    'C': 3.25,
    'D': 3.75,
    'E': 17.75,
    'F': 1.25,
    'G': 1.25,
    'H': 1.25,
    'I': 7.25,
    'J': 0.75,
    'K': 0.0,
    'L': 5.75,
    'M': 3.25,
    'N': 7.25,
    'O': 5.75,
    'P': 3.75,
    'Q': 1.25,
    'R': 7.25,
    'S': 8.25,
    'T': 7.25,
    'U': 6.25,
    'V': 1.75,
    'W': 0.0,
    'X': 0.0,
    'Y': 0.75,
    'Z': 0.0
}

# Exemple et questions
# 1

def analyze(ciphertext):
    ciphertext = ciphertext.replace(' ', '')
    frequencies = {}
    for c in ciphertext:
        if c not in frequencies.keys():
            frequencies[c] = 1
        else:
            frequencies[c] += 1

    for key in frequencies.keys():
        frequencies[key] = frequencies[key] * 100.0 / len(ciphertext)

    return frequencies

def sortDictionary(dic):
    return sorted(dic.items(), key=operator.itemgetter(1))[::-1]

def compare(dic1, dic2, ciphertext):
    key1 = ""
    key2 = ""

    for i in range(len(dic1)):
        key1 += dic1[i][0]
        key2 += dic2[i][0]

    print("CMIKUSQOFYPWTZEHLNJDAGXRVB", key2)

    return ciphertext.translate(ciphertext.maketrans("CMIKUSQOFYPWTZEHLNJDAGXRVB", key2))
