from numpy import *

def pgcd(a,b):
    if b==0:
        return a
    else:
        r=a%b
        return pgcd(b,r)

def inversible(matrix):
    det = int(linalg.det(matrix))
    return pgcd(det, 26) == 1

# Transpose la chaine de caractere en liste d'entiers associes
def transposerAscii(message):
    message = message.upper()
    digits = []
    for c in message:
        num = ord(c) - ord('A') # Lettre vers ASCII
        if num not in range(26): # Permet de savoir si le caractere est valide (entre A et Z)
            continue # Permet d'aller a la prochaine iteration sans executer les instructions
        digits.append(num)
    return digits

# Transpose liste d'entiers en la chaine associee
def transposerLettre(digits):
    message = ""
    for c in digits:
        char = chr(int(c) + ord('A')) # ASCII vers lettre
        message += char
    return message

def grouper(digits, n):
    d = [digits[i:i+n] for i in range(0, len(digits), n)]
    if len(d[-1]) == 1:
         d[-1].append(25)
    return d

def modMatInv(A,p):       # Finds the inverse of matrix A mod p
  n=len(A)
  A=matrix(A)
  adj=zeros(shape=(n,n))
  for i in range(0,n):
    for j in range(0,n):
      adj[i][j]=((-1)**(i+j)*int(round(linalg.det(minor(A,j,i)))))%p
  return (modInv(int(round(linalg.det(A))),p)*adj)%p

def modInv(a,p = 26):          # Finds the inverse of a mod p, if it exists
  # Si a est negatif on change la valeur de a 
  a %= 26
  for i in range(2,p):
    if (i*a)%p==1:
      return i

def minor(A,i,j):    # Return matrix A with the ith row and jth column deleted
    A=array(A)
    minor=zeros(shape=(len(A)-1,len(A)-1))
    p=0
    for s in range(0,len(minor)):
        if p==i:
            p=p+1
        q=0
        for t in range(0,len(minor)):
            if q==j:
                q=q+1
            minor[s][t]=A[p][q]
            q=q+1
        p=p+1
    return minor

def CrypterHill(plaintext, cle):
    # On verifie que la cle entree en parametre est bien une matrice carree
    if shape(cle)[0] != shape(cle)[1]:
        return

    k = shape(cle)[0]

    # On verifie que la taille de la cle est bien superieure a la taille du message a chiffrer
    if k > len(plaintext):
        return

    cipherText=[]
    paquets = grouper(transposerAscii(plaintext), k) # On groupe les indices associes
    for x in paquets:
        y = dot(cle, x) # On effectue le produit matriciel entre la cle et le groupe d'indices
        for c in y:
            cipherText.append(c % 26)
    return transposerLettre(cipherText) # On transpose la liste d'indices obtenue en une chaine de caractere

def DCrypteHill(cipherText, cle):
    # On verifie que la cle entree en parametre est bien une matrice carree
    if shape(cle)[0] != shape(cle)[1]:
        return

    k = shape(cle)[0]

# On verifie que la taille de la cle est bien superieure a la taille du message chiffrer
    if k > len(cipherText):
        return

    indicesMessage = transposerAscii(cipherText)
    groupeIndicesCode = grouper(indicesMessage, k)
    inverseCle = inverseMatrice(cle)
    plaintext = []
    for groupe in groupeIndicesCode:
        resultat = dot(groupe, inverseCle)
        for indice in resultat:
            plaintext.append(indice)

    return transposerLettre(plaintext)


# renvoie l'inverse de la matrice passee en parametre
def inverseMatrice(matriceAInverser):
    return dot(inverseDeterminant(matriceAInverser), matriceCofacteurs(matriceAInverser))


# renvoie l'inverse du determinant de la matrice passee en parametre
def inverseDeterminant(matrice):
    determinant = linalg.det(matrice)
    for i in range(2, 26):
        if (i * determinant % 26) == 1:
            return i

# La matrice des cofacteurs pour une matrice 2, 2
# La matrice des cofacteurs pour une matrice 2, 2
def matriceCofacteurs(matrice):
    k = shape(matrice)[0] # taille de la matrice

    if k not in range(2, 4):
        return

    if k == 2:
        a = matrice[0][0]
        b = matrice[0][1]
        c = matrice[1][0]
        d = matrice[1][1]
        m = [[d, -b],[-c, a]]

    elif k == 3:
        a = matrice[0][0]
        b = matrice[0][1]
        c = matrice[0][2]
        d = matrice[1][0]
        e = matrice[1][1]
        f = matrice[1][2]
        g = matrice[2][0]
        h = matrice[2][1]
        i = matrice[2][2]

        m = [
            [linalg.det([[e, f], [h, i]]), -1 * linalg.det([[d, f], [g, i]]), linalg.det([[d, e], [g, h]])],
            [-1 * linalg.det([[b, c], [h, i]]), linalg.det([[a, c], [g, i]]), -1 * linalg.det([[a, b], [g, h]])],
            [linalg.det([[b, c], [e, f]]), -1 * linalg.det([[a, c], [d, f]]), linalg.det([[a, b], [d, e]])]
        ]

    return m