#!/usr/bin/python
# -*- coding: utf-8 -*-

import Tkinter as tk
import mainApplication
from hillcipher.cryptanalysis import *


class KnownCipherWindow(tk.Frame):
	def __init__(self):
		self.master = tk.Tk()
		self.master.columnconfigure(0, weight=1)
		self.master.rowconfigure(0, weight=1)
		self.master.title("Attaque à clair connu")
		self.master.geometry("400x300")
		tk.Frame.__init__(self, pady=5, padx=10)
		self.message = tk.StringVar()
		self.key = tk.StringVar()
		self.grid(sticky="nesw")
		self.rowconfigure(0, weight=1)
		self.columnconfigure(0, weight=1)
		self.create_widgets()

	def create_widgets(self):
		self.backButton = tk.Button(self, text="Retour au menu", command=self.back)
		self.backButton.grid(row=0, sticky="we")

		self.messageLabel = tk.Label(self, textvariable=self.message)
		self.messageLabel.grid(row=1, pady=2, sticky="we")

		self.ciphertextLabel = tk.Label(self, text="Texte chiffré")
		self.ciphertextLabel.grid(row=2)

		self.ciphertextInput = tk.Entry(self)
		self.ciphertextInput.grid(row=3, sticky="nsew")

		self.plaintextLabel = tk.Label(self, text="Clair correspondant")
		self.plaintextLabel.grid(row=4)

		self.plainTextInput = tk.Entry(self)
		self.plainTextInput.grid(row=5, sticky="nsew")

		self.keyLengthLabel = tk.Label(self, text="Taille de la clé")
		self.keyLengthLabel.grid(row=6, sticky="nsew")

		self.keyLengthInput = tk.Spinbox(self, from_=2, to=3)
		self.keyLengthInput.grid(row=7, column=0, padx=10, sticky="w")
		
		self.cipherButton = tk.Button(self, text="DÉCHIFFRER", command=self.knownCipherHandler)
		self.cipherButton.grid(row=8, pady=10, sticky="nsew")

		self.keyLabel = tk.Label(self, text="Clé")
		self.keyLabel.grid(row=9)

		self.keyOutput = tk.Entry(self, textvariable=self.key)
		self.keyOutput.grid(row=10, sticky="nsew")

	def back(self):
		self.master.destroy()
		menuWindow = mainApplication.MainApplication()
		menuWindow.master.mainloop()

	def knownCipherHandler(self):
		#try:
		plaintext = self.purgeInput(self.plainTextInput.get())
		ciphertext = self.purgeInput(self.ciphertextInput.get())
		key = clairConnu(plaintext, ciphertext, int(self.keyLengthInput.get()))
		self.message.set("Clé trouvée !")
		self.key.set(str(key))
	#except Exception as e:
		#self.message.set(str(e))

	def purgeInput(self, plaintext):
		charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		cleanPlaintext = ""
		for c in plaintext:
			if c in charset:
				cleanPlaintext += c
		return cleanPlaintext