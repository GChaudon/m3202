from hillcipher.util import *
import json
import sys

def clairConnu(messageClair, messageChiffre, n=2):
    if len(messageClair) != len(messageChiffre):
        raise Exception("Longueur du message clair different de la longueur du message chiffre")

    groupeClair = grouper(messageClair, n)
    groupeChiffre = grouper(messageChiffre, n)

    for i in range(len(groupeClair)-(n-1)):
        chiffre = transpose([
            transposerIndices(groupeChiffre[i]),
            transposerIndices(groupeChiffre[i+1])
        ])

        clair = transpose([
            transposerIndices(groupeClair[i]),
            transposerIndices(groupeClair[i+1])
        ])
        
        if inversible(clair):
            return dot(chiffre, inverseMatrice(clair)) % 26

    raise Exception("Cle non trouve")

def analyserMots():
    mots = []
    motsPossibles = []
    with open('hillcipher/mots.txt') as wordsFile:
        mots = wordsFile.read().split(' ')
    return mots

def getBigrammesFrequencies():
    data = ""
    with open('hillcipher/bigrammes.json') as frequenciesFile:
        data = json.loads(frequenciesFile.read())
    return data

def analyserBigrammes(ciphertext):
    frequenciesRaw = getBigrammesFrequencies()
    frequencies = {}

    d = [ciphertext[i:i+2] for i in range(0, len(ciphertext), 2)]

    if len(d[-1]) == 1:
         d[-1].append(25)

    for bigramme in d:
        if bigramme not in frequencies.keys():
            frequencies[bigramme] = 0
        frequencies[bigramme] += 1

    # print(sorted(frequencies.iteritems(), key = lambda x : x[1])[::-1])

    for bigramme in frequencies.keys():
        frequencies[bigramme] = round(frequencies[bigramme] / float(len(ciphertext)), 4)

    frequencies = sorted(frequencies.iteritems(), key = lambda x : x[1])[::-1]
    print("frequencies : ")
    print(frequencies)
    
    frequenciesRaw = sorted(frequenciesRaw.iteritems(), key = lambda x : x[1])[::-1]
    print("frequenciesRaw : ")
    print(frequenciesRaw)
    print('----------------------------------------')
    mots = ""
    with open("hillcipher/mots-courants.txt") as fichier:
        mots = fichier.read().split(" ")
    best = ""
    bestcount = 0
    for i in range(len(d)-1):
        for j in range(7):
            if d[i] == frequencies[j][0]:
                segment = d[i] + d[i+1]
                for b1 in frequenciesRaw[:5]:
                    for b2 in frequenciesRaw:
                        try:
                            keyFound = clairConnu(b1[0] + b2[0], segment)
                            txt = DCrypteHill(ciphertext, keyFound)
                            count = 0
                            for mot in mots:
                                if mot in txt:
                                    if len(mot) >= 3:
                                        count += 1
                            if count >= bestcount:
                                best = txt
                                bestcount = count
                                print("Searching for best match ...")
                                print(txt)
                        except:
                            pass

    print(best)
    print(str(bestcount) + " mots match")