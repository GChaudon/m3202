#!/usr/bin/python
# -*- coding: utf-8 -*-

import Tkinter as tk
from cipherWindow import *
from decipherWindow import *
from knownCipherWindow import *

class MainApplication(tk.Frame):
	def __init__(self):
		self.master = tk.Tk()
		self.master.columnconfigure(0, weight=1)
		self.master.rowconfigure(0, weight=1)
		self.master.title("Chiffre de Hill")
		self.master.geometry("400x375")
		tk.Frame.__init__(self, pady=20, padx=10)
		self.pack()
		self.create_widgets()

	def create_widgets(self):
		self.chooseLabel = tk.Label(self, text="Veuillez choisir une action", width=30, pady=15)

		self.chooseLabel.pack()

		self.buttons = buttons = tk.Frame(self, pady=5)
		self.buttons.pack(fill=tk.BOTH)

		self.cipherButton = tk.Button(buttons, text="Chiffrer", command=self.cipherHandler, pady=10)
		self.cipherButton.grid(row=0, pady=5, sticky="nesw")

		self.decipherButton = tk.Button(buttons, text="Déchiffrer", command=self.decipherHandler, pady=10)
		self.decipherButton.grid(row=1, pady=5, sticky="nesw")

		self.knownCipherButton = tk.Button(buttons, text="Attaque à clair connu", command=self.knownCipherHandler, pady=10)
		self.knownCipherButton.grid(row=2, pady=5, sticky="nesw")

		self.frequencyAttackButton = tk.Button(buttons, text="Attaque par cryptanalyse statistique", command=self.frequencyAttackHandler, pady=10)
		self.frequencyAttackButton.grid(row=3, pady=5, sticky="nesw")

		self.exitButton = tk.Button(buttons, text="Quitter", command=self.master.destroy, pady=10)		
		self.exitButton.grid(row=4, pady=5, sticky="nesw")

	def cipherHandler(self):
		self.master.destroy()
		cipherWindow = CipherWindow()
		cipherWindow.master.mainloop()
	
	def decipherHandler(self):
		self.master.destroy()
		decipherWindow = DecipherWindow()
		decipherWindow.master.mainloop()

	def knownCipherHandler(self):
		self.master.destroy()
		decipherWindow = KnownCipherWindow()
		decipherWindow.master.mainloop()

	def frequencyAttackHandler(self):
		print("Attaque par cryptanalyse statistique")