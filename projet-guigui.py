#!/usr/bin/python3

from numpy import *
import operator

french_frequencies = {
    'A': 8.25,
    'B': 1.25,
    'C': 3.25,
    'D': 3.75,
    'E': 17.75,
    'F': 1.25,
    'G': 1.25,
    'H': 1.25,
    'I': 7.25,
    'J': 0.75,
    'K': 0.0,
    'L': 5.75,
    'M': 3.25,
    'N': 7.25,
    'O': 5.75,
    'P': 3.75,
    'Q': 1.25,
    'R': 7.25,
    'S': 8.25,
    'T': 7.25,
    'U': 6.25,
    'V': 1.75,
    'W': 0.0,
    'X': 0.0,
    'Y': 0.75,
    'Z': 0.0
}

# Exemple et questions
# 1

def pgcd(a,b):
    if b==0:
        return a
    else:
        r=a%b
        return pgcd(b,r)

def inversible(matrix):
    det = int(linalg.det(matrix))
    return pgcd(det, 26) == 1

# Transpose la chaine de caractere en liste d'entiers associes
def transposerAscii(message):
    message = message.upper()
    digits = []
    for c in message:
        num = ord(c) - ord('A') # Lettre vers ASCII
        if num not in range(26): # Permet de savoir si le caractere est valide (entre A et Z)
            continue # Permet d'aller a la prochaine iteration sans executer les instructions
        digits.append(num)
    return digits

# Transpose liste d'entiers en la chaine associee
def transposerLettre(digits):
    message = ""
    for c in digits:
        char = chr(int(c) + ord('A')) # ASCII vers lettre
        message += char
    return message

def grouper(digits, n):
    paquets = []
    count = 0
    tab=[]
    for d in digits:
        if count == 0 and len(tab) > 1:
            paquets.append(tab)
            tab=[]
        tab.append(d)
        count = (count + 1) % n

    for i in range(n - len(tab)):
        tab.append(25) 

    paquets.append(tab)
    return paquets

def modMatInv(A,p):       # Finds the inverse of matrix A mod p
  n=len(A)
  A=matrix(A)
  adj=zeros(shape=(n,n))
  for i in range(0,n):
    for j in range(0,n):
      adj[i][j]=((-1)**(i+j)*int(round(linalg.det(minor(A,j,i)))))%p
  return (modInv(int(round(linalg.det(A))),p)*adj)%p

def modInv(a,p):          # Finds the inverse of a mod p, if it exists
  for i in range(1,p):
    if (i*a)%p==1:
      return i
  raise ValueError(str(a)+" has no inverse mod "+str(p))

def minor(A,i,j):    # Return matrix A with the ith row and jth column deleted
    A=array(A)
    minor=zeros(shape=(len(A)-1,len(A)-1))
    p=0
    for s in range(0,len(minor)):
        if p==i:
            p=p+1
        q=0
        for t in range(0,len(minor)):
            if q==j:
                q=q+1
            minor[s][t]=A[p][q]
            q=q+1
        p=p+1
    return minor

def CrypterHill(plaintext, cle):
    if shape(cle)[0] != shape(cle)[1]:
        return

    k = shape(cle)[0]

    if k > len(plaintext):
        return

    ciphertext=[]
    paquets = grouper(transposerAscii(plaintext), k)
    for x in paquets:
        y = dot(cle, x)
        for c in y:
            ciphertext.append(c % 26)
    return transposerLettre(ciphertext)

def DCrypteHill(ciphertext, cle):
    if shape(cle)[0] != shape(cle)[1]:
        return

    k = shape(cle)[0]

    if k > len(ciphertext):
        return

    plaintext=[]
    paquets = grouper(transposerAscii(ciphertext), k)
    for y in paquets:
        tmp = modMatInv(cle, 26)
        x = dot(tmp, y)
        for c in x:
            plaintext.append(c % 26)
    return transposerLettre(plaintext)

def analyze(ciphertext):
    ciphertext = ciphertext.replace(' ', '')
    frequencies = {}
    for c in ciphertext:
        if c not in frequencies.keys():
            frequencies[c] = 1
        else:
            frequencies[c] += 1

    for key in frequencies.keys():
        frequencies[key] = frequencies[key] * 100.0 / len(ciphertext)

    return frequencies

def sortDictionary(dic):
    return sorted(dic.items(), key=operator.itemgetter(1))[::-1]

def compare(dic1, dic2, ciphertext):
    key1 = ""
    key2 = ""

    for i in range(len(dic1)):
        key1 += dic1[i][0]
        key2 += dic2[i][0]

    print("CMIKUSQOFYPWTZEHLNJDAGXRVB", key2)

    return ciphertext.translate(ciphertext.maketrans("CMIKUSQOFYPWTZEHLNJDAGXRVB", key2))

B = array([[2, 3], 
           [7, 5]])

C = array([[1, 3, 2],
           [5, 3, 2],
           [7, 2, 5]])

D = array([[1, 3, 3],
           [5, 3, 2],
           [7, 2, 5]])

message1 = "HILL"

# Question 1
print("B est inversible : {}".format(inversible(B)))

# Question 2
print("C est inversible : {}".format(inversible(C)))
print("D est inversible : {}".format(inversible(D)))

# Question 3
print("Code de " + message1 + " : " + str(transposerAscii(message1)))
print("Message de " + str(transposerAscii(message1)) + " : " + transposerLettre(transposerAscii(message1)))

print(CrypterHill('JE MAPELLE GUILLAUME', B))
print(DCrypteHill('UWGMWZRREIUB', [[9, 4], [5, 7]]))

ciphertext = "HEAPD YTWCB ZOQOM ISTNI ZOAIS CCGHE GJGNR CJKFK ZOMIR CTWFK SJEUN WMCMI VRQOM SYUFO TKAJY UGZPT IHQHK MHEIP HEBTV CWVGJ GNUPA UMFGN RCWVF KFKJZ PCRCS AWZPT CBEUQ HRFBI WFMFP TMFPT EXIPA LWXYU QDSCZ SECMS MINIT WNFDO OLPCG ZLPLK HQEUF KHHUQ YPKHK CTNZS ZDSOO IXMPT LSNNP CNFQO MSCMF EUQRL MICMS OTWMS WZIWH AIDEC OYDOV RYAVA KMSOW UMIBI LCLKA LKMMI STICN QSOYK PCZOX MMFYB UJQYJ KD FUJYJ DQLYHU GKY D QLQYJ ZQCQYI DQLYWKU XEXU XEXU"

print(compare(sortDictionary(analyze(ciphertext)), sortDictionary(french_frequencies), ciphertext))