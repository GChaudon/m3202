#!/usr/bin/python

from numpy import *
from hillcipher.util import *
from hillcipher.cryptanalysis import *

# key = [[3, 5], [1, 2]]
# key2 = [[9, 4], [5, 7]]

# plaintext = "MON GENERAL NOUS ATTAQUONS DEMAIN"
# plaintext2 = "CRYPTO DE HILL"
# plaintext3 = "CEST PAS MA FAUTE"

# ciphertext = CrypterHill(plaintext, key)
# ciphertext2 = CrypterHill(plaintext2, key2)
# ciphertext3 = CrypterHill(plaintext3, key2)

# print(ciphertext)
# print(ciphertext2)
# print(ciphertext3)
# try:
#     key_found = clairConnu("MONGENERAL", "CORZZETMDW")
#     key_found2 = clairConnu("CRYPTO", "IZQRTL")
#     key_found3 = clairConnu("CESTPA", "IMEPFX")

#     print(DCrypteHill(ciphertext, key_found))
#     print(DCrypteHill(ciphertext2, key_found2))
#     print(DCrypteHill(ciphertext3, key_found3))
# except:
#     print("La cle n'a pas ete trouvee")

# analyserMots()

ciphertext = ""
with open("hillcipher/ciphertext.txt") as fichier:
    ciphertext = fichier.read().replace(' ', '')

listeMots = analyserMots()

# for mot in listeMots:
#     try:
#         key_found = clairConnu(mot, "XEXU")
#         print(DCrypteHill(ciphertext, key_found))
#         print("**************************************************************")
#     except:
#         pass

# for a in range(26):
#     for b in range(26):
#         for c in range(26):
#             for d in range(26):
#                 key = [[a, b], [c, d]]
#                 if(inversible(key)):
# 	                decrypted = DCrypteHill(ciphertext, key)
# 	                for mot in listeMots:
# 						if(decrypted.count(mot) >= 3):
# 							print(mot)
# 							print(decrypted, key)
# 							print("-------------------------")

# ciphertext = ciphertext.replace('QM', 'RE')
# ciphertext = ciphertext.replace('NJ', 'ES')

# # print(ciphertext)
# roman = ""
# frequencies = {}
# with open('hillcipher/roman.txt') as romanFile:
# 	roman = romanFile.read()

# d = [roman[i:i+2] for i in range(0, len(roman), 2)]

# # if len(d[-1]) == 1:
# #      d[-1].append(25)

# for bigramme in d:
#     if bigramme not in frequencies.keys():
#         frequencies[bigramme] = 0
#     frequencies[bigramme] += 1

# # print(sorted(frequencies.iteritems(), key = lambda x : x[1])[::-1])

# for bigramme in frequencies.keys():
#     frequencies[bigramme] = round(frequencies[bigramme] / float(len(ciphertext)), 4)

# frequencies = sorted(frequencies.iteritems(), key = lambda x : x[1])[::-1]

# # print(frequencies)

# print("**********************")
# print("**********************")
# print("**********************")
# print("**********************")
# print("**********************")
# print("**********************")
# print("**********************")
# print("**********************")
# print("**********************")
# print("**********************")

frequencies = analyserBigrammes(ciphertext)
print(frequencies[0])
#print("Texte chiffre: ", ciphertext)
#cryptanalyseStatistique(ciphertext, frequencies[0], frequencies[1], depth=5)